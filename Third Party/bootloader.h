#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#include "main.h"

#define ADDRESS 0X08008000
#define SECTOR FLASH_SECTOR_2
#define ACTIVE_BOOT 0x01
#define DESACTIVE_BOOT 0x00

uint8_t status_Bootloader(void);
void active_Bootloader(void);
void desactive_Bootloader(void);

#endif
