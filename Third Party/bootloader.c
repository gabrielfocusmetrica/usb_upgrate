#include "bootloader.h"

/**
  * @brief This function return the state of bootloader 
  * @retval Return 1 if a upgrade firmware request is requerided, 0 if not is
  */
uint8_t status_Bootloader(void)
{
	uint32_t value, mem_address;
	//mem_address = 0x8008000;
	mem_address = ADDRESS;
	value = *(uint32_t*) mem_address;
	return (uint8_t) value;
}

/**
  * @brief This function put 1 in the bootloader memory, this way, when  MCU start can know how start the bootloader. 
	*				 In this case, the firmware must be updated.
  * @note After this function, "NVIC_SystemReset ()" function is requerided
  */
void active_Bootloader(void)
{
	HAL_FLASH_Unlock();
	FLASH_Erase_Sector(SECTOR, FLASH_VOLTAGE_RANGE_3);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, ADDRESS, ACTIVE_BOOT);
	HAL_FLASH_Lock();
}

/**
  * @brief This function put 0 in the bootloader memory, this way, when  MCU start can know how start the bootloader. 
	*				 In this case the current firmware must start to run 
  */
void desactive_Bootloader(void)
{
	HAL_FLASH_Unlock();
	FLASH_Erase_Sector(SECTOR, FLASH_VOLTAGE_RANGE_3);
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, ADDRESS, DESACTIVE_BOOT);
	HAL_FLASH_Lock();
}
